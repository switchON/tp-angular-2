

const uuidv4 = require('uuid/v4');
var express = require('express');
var app = express();


app.get('/api/uuids', function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    let uuid = uuidv4();

    res.send(JSON.stringify({
        uuid: uuid
    }));
});


app.get('/:path', function(req, res){
    let path = req.params["path"] ? req.params["path"] : 'index.html';

    res.sendFile(path, {root: './dist'}, (err) => {
        res.sendFile('index.html', {root: './dist'});
    });
});

app.get('/', function(req, res){
    res.sendFile('index.html', {root: './dist'});
});

app.listen(3000, console.log('Server is listening.'));