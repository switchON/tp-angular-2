import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule }   from '@angular/router';
import { AppComponent }   from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import MaterialModule from './material/material.module';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports:      [
    MaterialModule,
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(
      [
        {
          path: 'tasks',
          loadChildren: "./tasks/tasks.module#TasksModule"
        },{ 
          path:'',
          redirectTo: 'tasks',
          pathMatch: 'full' 
        }
      ]
    )
  ],
  declarations: [
    AppComponent
  ],
  bootstrap:    [
    AppComponent
  ],
  providers: []
})
export class AppModule { }
