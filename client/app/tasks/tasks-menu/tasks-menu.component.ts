import { Task } from "./../models";
import { TasksService } from '../tasks.service';
import { MatSnackBar } from '@angular/material';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tasks-menu',
  templateUrl: 'tasks-menu.component.html',
  styleUrls:  [
    'tasks-menu.component.css'
  ],
  providers: [
    TasksService 
  ]
})
export class TasksMenuComponent implements OnInit {
    
  /**
   * Constructor of the component.
   * @param taskService Service to communicate with the task API.
   * @param snackBar Material service displaying snackbar messages.
   */
  constructor(
    private taskService: TasksService,
    public snackBar: MatSnackBar
  ){}

  /* The array containing the tasks to display */
  private tasks: Array<Task> = [];
  /* The var which contains the opened task (loaded into the form) */
  private openedTask: object = null;

  ngOnInit(){
    /* Load the tasks when component is loading */
    this.refreshTasks();
  }

  /**
   * Refresh the list of tasks using the web service.
   */
  private refreshTasks(){
    /* TODO 1 : Subscribe to the list method (which returns an observable) implemented in the task service
    to populate the tasks variable */
    this.taskService.list().subscribe((tasks: Array<Task>) => {
        this.tasks = tasks;
    });
  }

  /**
   * Method called when the user triggers a task for deletion.
   * @param task The task to delete.
   */
  private deleteTask(task: Task){
    this.taskService.delete(task).subscribe((result) => {
      this.snackBar.open("Todo deleted.", "OK", {
          duration: 5000,
      });
      this.refreshTasks();
    });
  }

  /**
   * Triggered when the user clic on creation button.
   * Displays the task creation form.
   */
  private openTaskForm(){
    this.openedTask = {
        "days": null,
        "name": null
    };
  }

  /**
   * Called when the form is submitted. Create a task in the API.
   * @param task The task to create.
   */
  private createTask(task: Task){
    /* TODO 6 : Subscribe to the taskService save method and refresh
    the list when it's done */
    this.taskService.save(task).subscribe((result) => {
      this.snackBar.open("Todo saved.", "OK", {
          duration: 5000,
      });
      this.openedTask = null;
      this.refreshTasks();
    });
  }

  /**
   * Called when a user cancel the task creation.
   * Hides the creation form.
   */
  private cancelTaskCreation(){
    this.openedTask = null;
  }
}