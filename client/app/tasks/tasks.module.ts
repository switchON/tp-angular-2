import { NgModule }      from '@angular/core';
import { RouterModule }   from '@angular/router';
import { CommonModule }       from '@angular/common';
import { FlexLayoutModule } from "@angular/flex-layout";
import AppCommon from './../app-common/app-common.module';
import MaterialModule from './../material/material.module';
import { TaskFormComponent } from './task-form/task-form.component';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { TasksMenuComponent } from './tasks-menu/tasks-menu.component';
import { TasksListComponent } from './tasks-list/tasks-list.component';

@NgModule({
  imports:      [
    AppCommon,
    MaterialModule,
    CommonModule,
    FormsModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    RouterModule.forChild(
      [
        {
          path: '',
          component: TasksMenuComponent
        }
      ]
    )
  ],
  declarations: [ 
    TasksMenuComponent,
    TasksListComponent,
    TaskFormComponent
  ],
  exports: [],
  providers: [],
  entryComponents: []
})
export class TasksModule { }
