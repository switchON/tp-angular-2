import { 
  Component, 
  Input,
  OnInit,
  Output,
  EventEmitter
} from '@angular/core';
import { Task } from "./../models";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-task-form',
  templateUrl: 'task-form.component.html',
  styleUrls:  [
    'task-form.component.css'
  ]
})
export class TaskFormComponent {

  /**
   * Construct the form component.
   * @param fb The formBuilder object is a service used to construct forms.
   */
  constructor(
    private fb: FormBuilder
  ){}

  /* The task to edit */
  @Input() task: object;
  /* The event triggered when the form is submitted */
  @Output() taskSubmitted = new EventEmitter();
  @Output() cancelled = new EventEmitter();

  /* The Form group containing all the form rules for validation */
  private form : FormGroup;
  
  ngOnInit() {
    /* Init the form with validators. All fields are required, set to null by default */  
    this.form = this.fb.group({
      'name': [null, Validators.compose([Validators.required])],
      'description': [null], /* TODO 3 : Modify priority with 0 as default value, make it required and with a min 0 constraint */
      'priority': [0, Validators.compose([Validators.required, Validators.min(0)])], 
      'dueDate': [null]
    });
  }

  /**
   * Submit the form.
   * @param value The value extracted from the form.
   */
  private submitForm(value: object){
    /* Emit an event (object) with a task field */
    /* TODO 4 : Create a task object, and emit it to the parent component
    thanks to taskSubmitted EventEmitter. */
    if(this.form.valid){
      this.taskSubmitted.emit({
        "task": new Task(null, value['name'], value['description'], value['dueDate'], value['priority'])
      });
    }
  }

  /**
   * Cancel the task creation by emitting a cancel event.
   */
  private cancel(){
    this.cancelled.emit({});
  }
}