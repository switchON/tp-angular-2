export class Task {
    constructor(
        public uuid:string, 
        public name: string, 
        public description: string, 
        public dueDate: Date,
        public priority: number
    ){}
}