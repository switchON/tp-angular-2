import { Task } from './models'
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { LocalService } from './../app-common/local.service';
import { UUIDService } from './../app-common/uuid.service';


@Injectable()
export class TasksService {
  /**
   * This service handle task interactions.
   */
  
  constructor (
    private localService: LocalService,
    private uuidService: UUIDService
  ) {}

  /**
   * Take a task to save. Affect it a unique ID, then save it in local
   * storage.
   * @param task The task to save
   */
  save(task: Task): Observable<Task>{
    /* TODO 7: 
    - Call the UUID service to generate an UUID, 
    - Affect it to the task variable,
    - Load the list saved in local storage,
    - Add task to the list,
    - Save the updated list. */

    return this.uuidService.generate()
      .flatMap((uuid: string) => {
        task.uuid = uuid;
        return this.list();
      })
      .flatMap((tasks: Array<object>) => {
        tasks.push(task);
        return this.localService.set('tasks', tasks).map(() => {
          return task;
        })
      });
  }

  /**
   * List the tasks stored locally.
   */
  list(){
    return this.localService.get('tasks').map((result) => {
      return (result == null || typeof(result) === "undefined") ? [] : result;
    });
  };

  /**
   * Delete a task.
   * @param task The task to delete.
   */
  delete(task: Task){
    /* TODO 8 :
    - List the tasks stored locally,
    - Find the one which has the same UUID tham the task we want to delete,
    - Delete it from the list.
    - Sqve the updated list */

    return this.list()
      .flatMap((tasks: Array<Task>) => {
        for(let i = tasks.length-1; i >= 0; i--){
          if(tasks[i].uuid === task.uuid){
            tasks.splice(i, 1);
            break;
          }
        }
        return this.localService.set('tasks', tasks);
      });
  };
  
}
