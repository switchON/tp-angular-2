import { Task } from "./../models";
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-tasks-list',
  templateUrl: 'tasks-list.component.html',
  styleUrls:  [
    'tasks-list.component.css'
  ]
})
export class TasksListComponent {
  
  /* The list of tasks to display */
  @Input() tasks: Array<Task> = [];
  /* Event emitted when a user want to delete a task */
  @Output() taskDeleted = new EventEmitter();

  /**
   * Called when a user push on the delete button.
   * @param index The position in the array of the task to delete.
   */
  private deleteTask(index: number){
    this.taskDeleted.emit({
      task: this.tasks[index]
    });
  }

  /**
   * Format date for template.
   * @param date The date to format.
   */
  private formatDate(date: Date): string{
    if(date == null){
      return "";
    }
    return (date.getMonth()+1) + "/" + date.getDate() + "/" + date.getFullYear()
  }

}