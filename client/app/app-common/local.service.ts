import { Injectable } from '@angular/core';
import * as localForage from 'localforage';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class LocalService {
  /**
   * The service encapsulating local data processing.
   */

  /**
   * Get a value from the local storage.
   * @param key The key we want to fetch.
   */
  public get(key: string){
    return Observable.fromPromise(localForage.getItem(key));
  }

  /**
   * Set a value in the store associated to the specified key.
   * @param key The key to associate with the value.
   * @param value The value to set.
   */
  public set(key: string, value: any){
    return Observable.fromPromise(localForage.setItem(key, value));
  };

  /**
   * Delete a value from the local storage.
   * @param key The key we want to delete.
   */
  public delete(key: string){
    return Observable.fromPromise(localForage.removeItem(key));
  }
  
}
