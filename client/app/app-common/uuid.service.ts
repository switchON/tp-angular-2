import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class UUIDService {
  /**
   * UUID Service class.
   */
  
  private url = 'api/uuids/';

  constructor (
    private http: HttpClient
  ) {}

  /**
   *Generate a unique UUID coming from the server.
   */
  generate(){
    /* TODO : 
    - Do a GET request on api/uuids/ url to get a unique string
    - Use the handleError method to catch potential errors */

    return this.http.get(this.url).map((res: object) => {
        return res['uuid'];
      }).catch(this.handleError);
  }

  /**
   * Handle an error coming from the backend.
   * @param err The error to parse.
   */
  private handleError (err: any) {
    let errMsg;
    if (err.error instanceof Error) {
      // A client-side or network error occurred. Handle it accordingly.
      errMsg ='An error occurred:' + err.error.message;
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      errMsg = "Backend returned code " + err.status + ", body was: " + JSON.stringify(err.error);
    }
    console.error(errMsg);
    return Observable.throw(err.error);
  };
}
