import { NgModule }      from '@angular/core';
import { LocalService } from "./local.service";
import { UUIDService } from "./uuid.service";
@NgModule({
  imports:      [],
  declarations: [],
  bootstrap:    [],
  providers: [
    LocalService,
    UUIDService
  ]
})
export default class AppCommonModule {}
