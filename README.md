

## Setup

Dependencies for node (if linux).

```bash
curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash -
sudo apt-get install -y nodejs
```

To setup the project.
```bash
npm install
```

## Start dev environment

Start the dev server.
```bash
nodejs server.js
```
Start the webpack watch mode (compilation).
```bash
nodejs server.js
```
